========================================
資料庫 (Database)
========================================


資料庫案例：

* SQL
    - `SQLite <sqlite.rst>`_
    - `MariaDB <mariadb.rst>`_
    - MySQL
    - PostgreSQL
* NoSQL
    - `Redis <redis.rst>`_
    - `MongoDB <mongodb.rst>`_
    - `LevelDB <leveldb.rst>`_
    - CouchDB
    - RethinkDB
    - RocksDB
* NewSQL
    - `CockroachDB <cockroachdb.rst>`_
    - Google Spanner
    - `TiDB <tidb.rst>`_


資料結構：

* LSM tree (log-structured merge-tree)
* B+ tree


演算法：

* `Consensus Algorithm <consensus-algorithm.rst>`_
    - Paxos
        + Chubby (Google)
    - Raft
        + TiKV
        + etcd
