========================================
SIMD
========================================


.. contents:: 目錄


參考
========================================

* `AVX-512: when and how to use these new instructions – Daniel Lemire's blog <https://lemire.me/blog/2018/09/07/avx-512-when-and-how-to-use-these-new-instructions/amp/>`_
    - SIMD tradeoff
