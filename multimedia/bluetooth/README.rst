========================================
藍牙相關 (Bluetooth)
========================================

* `藍牙相關音訊編碼知識 (Bluetooth Codecs) <bluetooth-codecs-concept.rst>`_
* SBC
* aptX
* `LDAC codec <ldac.rst>`_
