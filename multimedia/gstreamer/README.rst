========================================
GStreamer
========================================

* 基本介紹、設計概念
* `編譯 <build.rst>`_
* 內部溝通機制
* 撰寫 Plugin
* Bus
* Pad Probe
* Signal
* Transcoding
* `Transmuxing <transmuxing.rst>`_
* Transrating
* Transsizing
* Adaptive Streaming
* File Repair
* 其他程式語言 Binding
    - gobject-introspection
    - typelibs
    - Python
        + PyGObject
        + gst-python
        + GStreamer + asyncio
        + Signal/Promise/Callback
    - Rust
* WebRTC
* UPnP streaming
* DLNA
* Miracast
* Server
* Filter
* `上字幕 (Subtitle) <subtitle.rst>`_
* `串流混合 (Mixing/Composition) <mixing.rst>`_
* `硬體加速 (Hardware Acceleration) <hardware-acceleration.rst>`_
* `藍牙 (Bluetooth) <bluetooth.rst>`_
* `Google Cast <googlecast.rst>`_
* `播放器 (Player) <player.rst>`_
* `網頁顯示 (Web) <web.rst>`_
* `Cheatsheet <cheatsheet.rst>`_
* `GStreamer 相關公司 <company.rst>`_
* 使用 GStreamer 的專案
    - Ardour
    - Pitivi
    - Cheese
    - GNOME Videos
    - Rhythmbox
