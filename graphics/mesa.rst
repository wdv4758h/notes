========================================
Mesa
========================================


.. contents:: 目錄


參考
========================================

* `A case for a faster and simpler driver - NIR on the Mesa i965 backend <https://fosdem.org/2016/schedule/event/i965_nir/attachments/slides/1113/export/events/attachments/i965_nir/slides/1113/nir_vec4_i965_fosdem_2016_rc1.pdf>`_
* `除去 Mesa 內 99% 的編譯器警告 <https://lists.freedesktop.org/archives/mesa-dev/2018-September/205748.html>`_
